;(function() {
	let url = 'http://www.mocky.io/v2/5ceb102e330000fe467c394d';
	let json = res => res.json();
	let status = res => {
		if (res.status !== 200) {
			return Promise.reject(new Error(res.statusText))
		}
		return Promise.resolve(res)
	};

	fetch(url)
	.then(status)
	.then(json)
	.then(data => {
		init(data);
	})
	.catch(error => console.log(error));

	function init(data) {
		/* object state app*/
		const state = {
			filterFlat        : false,
			filterPrice       : false,
			itemsLengthInit   : 12,
			itemsShowStep     : 20,
			itemsLengthCurrent: 20,
			itemsLengthTotal  : null,
			getNews           : true
		};
		/* end object state app*/

		const items =  data;
		const $body = document.body;
		const $header = $body.querySelector('.header');
		const $footer = $body.querySelector('.footer');
		const $cards = $body.querySelectorAll('.card');
		const $suggestions = $body.querySelector('#suggestions');
		const btnMore = $body.querySelector('[data-more]');
		const $mobMenu = $body.querySelector('[data-mob-menu]');
		const $mobMenuOpen = $header.querySelector('[data-mob-menu-open]');
		const $form = $footer.querySelector('[data-formWithValidation]');
		const $fields = $form.querySelectorAll('.js-field');
		const $checkbox = $form.querySelector('[data-check]');
		const $sort = $body.querySelector('.sort');
		state.itemsLengthTotal = items.length;
		let itemsRendered = items.slice(0, state.itemsLengthInit);

		/* rendering items */
		rendering(itemsRendered);
		renderingButtonMore();
		setlocalStorage('items', items);
		/* end rendering items */

		/* clickable */
		$cards.forEach(card => card.addEventListener('click', handlerCardClick));
		/* end clickable */

		/* show more cards */
		btnMore.addEventListener('click', handleMoreButtonClick);
		/* end show more cards */

		/* sorting */
		$sort.addEventListener('click', handlerSortClick);
		/* end sorting */

		/* scroll to top */
		const basicScrollTop = () => {
			// The button
			const btnTop = document.querySelector('#scrollToTop');

			const btnReveal = () => {
				if (window.scrollY >= 300) {
					btnTop.classList.add('is-visible');
				} else {
					btnTop.classList.remove('is-visible');
				}
			};

			const TopscrollTo = () => {
				if (window.scrollY != 0) {
					setTimeout(() => {
						window.scrollTo(0, window.scrollY - 30);
						TopscrollTo();
					}, 5);
				}
			};

			// Listeners
			window.addEventListener('scroll', btnReveal);
			btnTop.addEventListener('click', TopscrollTo);

		};
		basicScrollTop();
		/* end scroll to top */

		/* validation */
		$form.addEventListener('submit', handlerFormSubmit);

		$fields.forEach((field) => {
			field.addEventListener('input', function() {

				(field.value !== "") ? removeValidation('.js-error') : checkFieldsPresence();
			})
		});
		/* end validation */

		/* drop menu */
		$mobMenu.addEventListener('click', handlerMobMenuCloseClick);

		$mobMenuOpen.addEventListener('click', handlerMobMenuOpenClick);
		/* end drop menu */

		/* rendering checked email field */
		$checkbox.setAttribute('checked', state.getNews);

		$checkbox.addEventListener('click', handleCheckboxChecked);
		/* end rendering checked email field */

		/* FUNCTIONS */
		// callback form
		function handlerFormSubmit(event) {
			event.preventDefault();

			removeValidation('.js-error');
			checkFieldsPresence();
		}

		// callback handlerCardClick
		function handlerCardClick(event) {
			event.preventDefault();

			let target = event.target;

			while (target !== this) {
				if (target.classList.contains("card__favorite")) {
					if (target.classList.contains("js-active")) {
						target.classList.remove('js-active');

						return;
					}
					target.classList.add('js-active');
					return;
				}
				target = target.parentNode;
			}
		}

		// callback sort
		function handlerSortClick(event) {
			let target = event.target;

			while (target !== this) {

				if (target.hasAttribute('data-sort-price')) {

					state.filterPrice ? itemsRendered.sort(byProp("price", true)) : itemsRendered.sort(byProp("price", false));
					state.filterPrice = !state.filterPrice;

					if (state.filterPrice) {
						target.classList.add('js-active');
					} else {
						target.classList.remove('js-active');
					}

					clearing();
					rendering(itemsRendered);
					return;
				}

				if (target.hasAttribute('data-sort-flat')) {

					state.filterFlat ? itemsRendered.sort(byProp("sqrt", true)) : itemsRendered.sort(byProp("sqrt", false));
					state.filterFlat = !state.filterFlat;

					target.parentNode.querySelectorAll('.js-active').forEach(item => {
						item.classList.remove('js-active');
					});

					if (state.filterFlat) {
						target.classList.add('js-active');
					} else {
						target.classList.remove('js-active');
					}

					clearing();
					rendering(itemsRendered);
					return;
				}

				target = target.parentNode;
			}
		}

		// callback mobMenuClose
		function handlerMobMenuCloseClick(event) {
			let target = event.target;

			while (target !== this) {
				if (target.hasAttribute('data-mob-menu-close')) {
					$body.classList.remove('js-open-menu');
					$body.setAttribute('style',
						'overflow: initial; padding-right: 0');

					return;
				}

				target = target.parentNode;
			}
		}

		// callback mobMenuOpen
		function handlerMobMenuOpenClick() {
			let scrollWidth = window.innerWidth - document.body.clientWidth;

			$body.classList.add('js-open-menu');

			$body.setAttribute('style',
				'overflow: hidden; padding-right: ' + scrollWidth + 'px')
		}

		// callback checkbox
		function handleCheckboxChecked(event) {
			let target = event.target;

			while (target !== this) {
				if (target.getAttribute("id") === 'inputSignUp') {
					target.setAttribute('checked', !state.getNews);
					state.getNews = !state.getNews;
					return;
				}

				target = target.parentNode;
			}
		}

		// callback btnMore
		function handleMoreButtonClick() {
			let total = items.length;
			let current = itemsRendered.length;
			let step = state.itemsShowStep;
			let matches = total - current;

			if (matches >= step) {
				itemsRendered = itemsRendered.concat(items.slice(current, current + step));

				clearing();
				rendering(itemsRendered);

			} else if (step > matches && matches !== 0) {
				itemsRendered = itemsRendered.concat(items.slice(current, current + matches));
				btnMore.remove();

				clearing();
				rendering(itemsRendered);
			}

			renderingButtonMore();
		}

		// insert card
		function rendering(data) {

			let result = '';

			data.forEach(item => {
				result += templateCard(item);
			});

			$suggestions.innerHTML = result;
		}

		function renderingButtonMore() {
			let total = items.length;
			let current = itemsRendered.length;
			let step = state.itemsShowStep;
			let matches = (total - current) >= step ? step : total - current;
			btnMore.querySelector('span').innerHTML = matches;
		}

		// clear all cards
		function clearing() {
			$cards.forEach((card) => {
				card.remove();
			});
		}

		// sorting object by number
		function byProp(prop, reverse) {
			return function(a, b) {

				if (a[prop] < b[prop]) return reverse ? 1 : -1;
				if (a[prop] > b[prop]) return reverse ? -1 : 1;
			}
		}

		function validateEmail(data) {
			const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

			return reg.test(data) !== false;
		}

		// create error validation
		function generateError(text) {
			const error = document.createElement('div');
			error.className = "js-error";
			error.innerHTML = text;
			return error;
		}

		// delete error validation
		function removeValidation(errorClass) {
			const $errors = $form.querySelectorAll(errorClass);

			$errors.forEach(error => error.remove());
		}

		//validate form fields by success and error
		function checkFieldsPresence() {
			$fields.forEach(field => {

				if (!field.value) {
					let error = generateError('Cannot be blank');
					field.parentElement.appendChild(error);
					field.classList.add('js-invalid');

				} else if (!validateEmail(field.value)) {
					let error = generateError('Incorrect email');
					field.parentElement.appendChild(error);
					field.classList.add('js-invalid');

				} else {
					field.classList.remove('js-invalid');
					alert("Submited!");
				}
			})
		}

		/* END FUNCTIONS */

		// save localstorage
		function setlocalStorage(name, key) {
			return localStorage.setItem(name, JSON.stringify(key))
		}

		function getlocalStorage(key) {
			return JSON.parse(localStorage.getItem(key))
		}

		function templateCard(item) {
			return `
			<div class="card-grid__cell">
			   <div class="card" data-active="${item.isActive}">
						<div class="card__desc">
							<span class="card__info"
							      style="display: ${item.massage.discount ? 'block' : 'none'}">${item.massage.discount ? item.massage.discount : null}</span>
							<span class="card__info"
								    style="display: ${item.massage.shock_price ? 'block' : 'none'}">${item.massage.shock_price ? item.massage.shock_price : null}</span>
			
							<a href="" class="card__favorite" style="display:${item.favorite.favoriteInit ? 'block' : 'none'}">
			         <svg width="19" height="18" role="img" aria-hidden="true">
				         <use xlink:href="#star"></use>
			         </svg>
							</a>
						</div>
						<!-- /.card__desc -->
			
						<picture class="card__media">
							<img src="${item.img.link}" alt="${item.img.alt}">
						</picture>
						<!-- /.card__media -->
			
						<div class="card__content">
			
							<h3 class="card__named">${item.named}</h3>
			
							<div class="card__definition definition">
				        <p class="definition__state">${item.definition.repair}</p>
			          <p>
					        <span class="definition__num">${item.sqrt}</span>
					        <span class="definition__unit">м<sup>2</sup></span>
			            <br/>
			            <small>площадь</small>
			          </p>
					      <p>
						      <span class="definition__num">${item.definition.floor}</span>
									<br/>
									<small>этаж</small>
								</p>
							</div>
			
							<p class="card__price">${item.price.toLocaleString()} руб.</p>
			
						</div>
						<!-- /.card__content -->
			
						<button type="button" class="card__btn btn btn-block"
				        data-status="${item.statesBtn.dataStatus}"
				        ${item.statesBtn.dataStatus === 'sales' ? 'disabled' : ''}>${item.statesBtn.state}</button>
			</div>
			<!-- /.card -->
	
		</div>
			<!-- /.card-grid__cell -->
		`;
		}
	}
}());
